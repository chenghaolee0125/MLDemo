from django.test import TestCase
from django.urls import resolve

from posts.views import home


class TestHomePageView(TestCase):

    def test_get_home_page(self):
        # get url localhost:8000/
        response = self.client.get('/')

        # check which template is used
        self.assertTemplateUsed(response, 'home.html')

        # check response status is equal to 200
        self.assertEqual(response.status_code, 200)