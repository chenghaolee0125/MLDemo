from selenium import webdriver
from django.test import TestCase


class FunctionalTest(TestCase):

    # do something before test start
    def setUp(self):
        self.browser = webdriver.Firefox()

    # do something after test complete
    def tearDown(self):
        self.browser.quit()

    # our test function
    def test_get_url(self):
        self.browser.get('http://localhost:8000')
        assert 'home' in self.browser.title