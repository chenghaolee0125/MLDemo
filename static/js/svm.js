var index = 0;
var colors = ["#0066FF", "#00DD00"];
var grid_colors = ["#99CCFF", "#99FF99"]
var svr_line_color = "white";
var eps_tube_color = "blue";
var context = null;
var width = null;
var height = null;
var train_points = [];


function setColorButton() {
    var color = document.getElementById("color");
    color.style.backgroundColor = colors[index];
    color.style.color = "black";
}

function drawPoint(x, y, color, size_x=4, size_y=4) {
    context.fillStyle = color;
    context.fillRect(x, y, size_x, size_y);
}


function addPoint(x, y) {
    drawPoint(x, y, colors[index]);
    train_points.push([index, (x / width).toFixed(3), (y / height).toFixed(3)]);
}

function init() {
    setColorButton();
    var canvas = document.getElementById('features');
    width = canvas.width;
    height = canvas.height;
    context = canvas.getContext("2d");
    canvas.onclick = function (e) {
        // var mouseX = e.pageX - this.offsetLeft;
        // var mouseY = e.pageY - this.offsetTop;
        var posi = canvas.getBoundingClientRect();
        var mouseX = e.pageX - posi.left - window.scrollX;
        var mouseY = e.pageY - posi.top - window.scrollY;
        // console.log(e.pageX, posi.left, e.pageY, posi.top)
        addPoint(mouseX, mouseY);
    };
}

function nextColor() {
    index = (index + 1) % colors.length;
    setColorButton();
}

function clearScreen() {
    context.clearRect(0, 0, width, height);
}

function clearScreenAndData() {
    clearScreen();
    train_points = [];
}


function drawModel() {
    
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }
    var csrftoken = getCookie('csrftoken');
    
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }
    $.ajaxSetup({
        crossDomain: false, // obviates need for sameOrigin test
        beforeSend: function(xhr, settings) {
            if (!csrfSafeMethod(settings.type)) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });


    // var param = document.getElementById("param").value;
    // var prob = generateTrainData();
    // var model = libsvm_train_for_toy(prob, param); // free problem

    // $.get("/add/", { 'train_points': JSON.stringify(train_points) }, function (ret) {
    //   console.log(ret);
    // })
    
    var C = document.getElementById("C").value;
    var kernel = document.getElementById("kernel").value;

    //   $.post("/run_logistic/", { 'train_points': JSON.stringify(train_points), 'lr': lr, 'iter': iter }, function (ret) {
    //     // var myRe = /(\-?\d.\d{3})/;
    //     // var myArray = myRe.exec(ret);
    //     drawline(ret);
    //     // alert(ret);
    //     console.log(ret[0],ret[1],ret[2]);
    //   })

    $.ajax({
        url: "/run_svm/",
        type: 'post',
        data: { 'train_points': JSON.stringify(train_points), 'C': C, 'kernel': kernel },
        dataType: 'json',
        success: function (ret) {
            drawgrid(ret);
            //     // alert(ret);
            //     console.log(ret[0],ret[1],ret[2]);
        }
    })

}

function drawgrid(ret) {

    for (i = 0; i < ret.length; i++) {
        drawPoint(ret[i][0][0]*width, ret[i][0][1]*height, grid_colors[ret[i][1]]);
      }
    
    for (i = 0; i < train_points.length; i++) {
        drawPoint(train_points[i][1]*width, train_points[i][2]*height, colors[train_points[i][0]]);
      }
    // context.beginPath();
    // context.moveTo(0, -ret[0] / ret[2] * height);
    // context.lineTo(1 * width, (- ret[0] - ret[1]) / ret[2] * height);
    // context.stroke();
    // alert(ret);
}

init();


