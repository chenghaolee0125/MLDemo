var train_points = [];

$(document).ready(function () {
    // 求和 a + b
    $("#sum").click(function () {
        
        $.get("/add/", { 'train_points': JSON.stringify(train_points) }, function (ret) {
            $('#result').html(ret);
        })
    });

    $("#next").click(function () {
        var a = $("#a").val();
        var b = $("#b").val();

        train_points.push([a, b])
    });

    // 列表 list
    $('#list').click(function () {
        $.getJSON('/ajax_list/', function (ret) {
            //返回值 ret 在这里是一个列表
            for (var i = ret.length - 1; i >= 0; i--) {
                // 把 ret 的每一项显示在网页上
                $('#list_result').append(' ' + ret[i])
            };
        })
    })

    // 字典 dict
    $('#dict').click(function () {
        $.getJSON('/ajax_dict/', function (ret) {
            //返回值 ret 在这里是一个字典
            $('#dict_result').append(ret.twz + '<br>');
            // 也可以用 ret['twz']
        })
    })
});