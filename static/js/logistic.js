var index = 0;
var colors = ["#0066FF", "#00DD00"];
var svr_line_color = "white";
var eps_tube_color = "blue";
var context = null;
var width = null;
var height = null;
var train_points = [];


function setColorButton() {
  var color = document.getElementById("color");
  color.style.backgroundColor = colors[index];
  color.style.color = "black";
}

function drawPoint(x, y, color) {
  context.fillStyle = color;
  context.fillRect(x, y, 4, 4);
}


function addPoint(x, y) {
  drawPoint(x, y, colors[index]);
  train_points.push([index, (x / width).toFixed(3), (y / height).toFixed(3)]);
}

function init() {
  setColorButton();
  var canvas = document.getElementById('features');
  width = canvas.width;
  height = canvas.height;
  context = canvas.getContext("2d");
  canvas.onclick = function (e) {
    // var mouseX = e.pageX - this.offsetLeft;
    // var mouseY = e.pageY - this.offsetTop;
    var posi = canvas.getBoundingClientRect();
    var mouseX = e.pageX - posi.left - window.scrollX;
    var mouseY = e.pageY - posi.top - window.scrollY;
    console.log(e.pageX, posi.left, e.pageY, posi.top)
    addPoint(mouseX, mouseY);
  };
}

function nextColor() {
  index = (index + 1) % colors.length;
  setColorButton();
}

function clearScreen() {
  context.clearRect(0, 0, width, height);
}

function clearScreenAndData() {
  clearScreen();
  train_points = [];
}


function drawModel() {
  // var param = document.getElementById("param").value;
  // var prob = generateTrainData();
  // var model = libsvm_train_for_toy(prob, param); // free problem
  // $.get("/add/", { 'train_points': JSON.stringify(train_points) }, function (ret) {
  //   console.log(ret);
  // })
  var lr = document.getElementById("lr").value;
  var iter = document.getElementById("iter").value;

  $.get("/run_logistic/", { 'train_points': JSON.stringify(train_points), 'lr': lr, 'iter': iter }, function (ret) {
    // var myRe = /(\-?\d.\d{3})/;
    // var myArray = myRe.exec(ret);
    drawline(ret);
    // alert(ret);
    console.log(ret[0],ret[1],ret[2]);
  })
}

function drawline(ret) {

  context.beginPath();
  context.moveTo(0, -ret[0] / ret[2] * height);
  context.lineTo(1 * width, (- ret[0] - ret[1]) / ret[2] * height);
  context.stroke();
  // alert(ret);
}

init();


