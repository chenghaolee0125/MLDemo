"""MLDemo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from django.contrib.staticfiles.storage import staticfiles_storage
from posts.views import home, home_test, svm, logistic, randomforest, test, run_logistic, run_svm, add, ajax_dict, ajax_list
from django.views.generic.base import RedirectView

urlpatterns = [
    path('admin', admin.site.urls),
    path('', home, name='home'),
    path('svm', svm, name='svm'),
    path('logistic', logistic, name='logistic'),
    path('randomforest', randomforest, name='randomforest'),
    path('home_test', home_test, name='home_test'),
    path('test', test, name = 'test'),
    # re_path(r'^favicon\.ico$',RedirectView.as_view(url='/static/src/favicon.ico')),
    path('favicon.ico', RedirectView.as_view(url=staticfiles_storage.url('/src/favicon.ico'))),
    re_path(r'^run_logistic/$', run_logistic, name = 'run_logistic'),
    re_path(r'^run_svm/$', run_svm, name='run_svm'),
    re_path(r'^add/$', add, name='add'),
    re_path(r'^ajax_list/$', ajax_list, name='ajax-list'),
    re_path(r'^ajax_dict/$', ajax_dict, name='ajax-dict'),
]
