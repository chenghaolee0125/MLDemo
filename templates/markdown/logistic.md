# **Logistic Regression**
## **Intro.**
<br>

> ### **標籤為零時其假說值也為零，反之標籤為一時其假說值為一**  

<br>

Logistic Regression 繼承了 Linear Regression 的假說，大致上概念接近，但針對了離群值易對 \*假說值 計算造成錯誤影響作出了修正，假說值計算方式的改動也連帶改變了 Cost Function的計算方式。最終其解決了 Linear Regression 的假說缺陷。  
  
<br>  
  
> ### **Hypothesis value approaches to one if, and only if, its label is one. Similarly, hypothesis value is zero when its label is zero**  

<br>  
  
Logistic Regression basically has the same hypothesis as Linear Regression in binary classification. However, Logistic Regression has the different way to calculate hypothesis value to correct the distorted hypothesis value when outliers present. Along with hypothesis formula modification, the cost function also changes.

<br>  
  
**\* Hypothesis value = H(x) = &theta;<sup>T</sup>X**

<br>

何謂 **離群值對假說值造成影響呢？** 還記得我們目前的假說嗎。若我們真的能找出一個計算假說值的方式能達成此目的，那麼即可利用閥值作為二分法分類的依據，以圖一為例。但若有一個離群值出現在數據中，在 Linear Regression 的假說值計算方式並沒有特別處理這個問題。當標籤為一的母體數據努力提高自己的假說值使其越來越接近 1 的同時，標籤為一的離群值卻因為假說值遠大於 1 而阻礙母體群的收斂 ( 收斂的方向不同 )，標籤為零的狀況反之亦然。其關鍵原因就在於 Linear Regression 並沒有考慮到假說值有可能會大於一或小於零。
  
<br>  
  
**How comes outliers influence the hypothesis value calculation?** When implementing binary classification using linear regression, we can use the threshold value to distinguish different classes. However, when outliers present, Linear Regression doesn't take care of this scenario. In fact, the outliers might have hypothesis value greater than 1 or smaller than 0. During fitting iterations, the hypothesis values of outliers converge to the totally opposite direction.

<br>

---
## **Sigmoid (Logistic) Function**

<br>  

要怎麼修正 Linear Regression 對於離群值計算的缺陷呢？

<br>

> **假說值原本會超過 [0, 1] 的範圍，阿你就不要讓他超過就好了啊**  

<br>

有沒有可能剛好存在一個非線性公式在 X>0 且趨近於無限大時 Y 值會趨近於 1 ，而 X<0 且趨近於負無限大時 Y 值會趨近於 0 呢？  

<br>

**Sigmoid (Logistic) Function**  
**h<sub>&theta;</sub>(x) = g(&theta;<sup>T</sup>X)**  
**g(z) = 1/(1+e^-z);**  

<br>

將原本的 Linear Regression 假說值計算方式，再額外多一個非線性轉換的步驟，即可獲得我們想要的 Property ，當 	&theta;<sup>T</sup>X 經過Sigmoid公式轉換後，假說值無論如何一定會落在 [0, 1] 之間 

<br>  

How to handle the problem of Linear Regression when outliers exist?  
  
<br>  
  
> **If hypothesis value exceeds the range [0, 1], don't let it be !!!**  

<br>  

Is there any chance to transform infinity to one and minus infinity to 0 ? Drum roll please. Let me present you Sigmoid (Logistic) function.  
  
<br>

**Sigmoid (Logistic) Function**  
**h<sub>&theta;</sub>(x) = g(&theta;<sup>T</sup>X)**  
**g(z) = 1/(1+e^-z);**  

<br>

Transform the origin formula by applying sigmoid function. Now, we have the desired properties. After transforming, no matter what, the hypothesis value always falls between [0, 1].

<br>  

---
## **Cost Function**

原本linear regression 版本的cost function 為  

**J(&theta;) = 1/m &sum; (h<sub>&theta;</sub>(x<sup>i</sup>) - y)<sup>2</sup>**  

如果直接把sigmoid function帶進去，其公式就不會是convex，對於收斂到最小值的過程會造成很大的麻煩，因此我們需要修正cost function改為  
<br>  

**Cost( h<sub>&theta;</sub>(x), y ) = -y log( h<sub>&theta;</sub>(x) ) - (1-y) log( 1-h<sub>&theta;</sub>(x) )**  
**J(&theta;) = -1/m [ &sum; y<sup>i</sup> log h<sub>&theta;</sub>(x<sup>i</sup>) + (1-y<sup>i</sup>) log (1-h<sub>&theta;</sub>(x<sup>i</sup>)) ]**

當標籤為 1 時，Cost只有在假說值也為 1 的時候Cost才會為 0 ，同時若標籤為 1 ，但假說值接近 0 ，Cost 就會接近無限大。  
<br>
為了找到最小的Cost，可以採用 Gradient Descent 的方法  
**&theta;<sub>j</sub> = &theta;<sub>j</sub> - &alpha; &part; J(&theta;)**  
**&theta;<sub>j</sub> = &theta;<sub>j</sub> - &alpha; &sum; (h<sub>&theta;</sub>(x<sup>i</sup>) - y<sup>i</sup>) x<sup>i</sup><sub>j</sub>**

<br>

**\*\* 可以注意一下新版的Cost Function，其形式長的就跟Maximum Likelihood一樣**  
<br>  
  
The origin version of the cost function for linear regression is:  

**J(&theta;) = 1/m &sum; (h<sub>&theta;</sub>(x<sup>i</sup>) - y)<sup>2</sup>**  

The derivative of the cost function in the new term (applying sigmoid funciton) is not a convex space. Hence, we need a new cost function.

<br>  

**Cost( h<sub>&theta;</sub>(x), y ) = -y log( h<sub>&theta;</sub>(x) ) - (1-y) log( 1-h<sub>&theta;</sub>(x) )**  
**J(&theta;) = -1/m [ &sum; y<sup>i</sup> log h<sub>&theta;</sub>(x<sup>i</sup>) + (1-y<sup>i</sup>) log (1-h<sub>&theta;</sub>(x<sup>i</sup>)) ]**
  
<br>
We can use Gradient Descent to minimize the cost function.  
  
**&theta;<sub>j</sub> = &theta;<sub>j</sub> - &alpha; &part; J(&theta;)**  
**&theta;<sub>j</sub> = &theta;<sub>j</sub> - &alpha; &sum; (h<sub>&theta;</sub>(x<sup>i</sup>) - y<sup>i</sup>) x<sup>i</sup><sub>j</sub>**

<br>

**\*\* New version of the cost function looks similar to Maximum Likelihood.**  

---
## **Demonstration**
<br>  

* 點擊左側空白區域新增資料點
* 如欲切換為另一顏色，點擊 change 按鈕，並繼續新增資料點
* 輸入 Learning Rate 和 Iteration 以觀察不同收斂結果
* 點擊 Run 按鈕顯示計算結果
* 如欲清除資料點擊計算結果，請點擊 Clear  
  
<br>  
  
* hit the left hand side blank region to add data.
* Change to another color by clicking the change button. And keep adding data.
* Edit Learning Rate and Iteration to observe different results.
* Click Run to show the results.
* Click Clear to clear data and results.


---
## **Future Work**
**TODO: Multiclass**
