# **Support Vector Machine**
## **Intro.**
<br>

> ### **修正羅吉斯回歸的成本函數且不再硬性強制假說值介於 [0,1]**  

<br>

SVM的細節很多， hard-margin, soft-margin, kernel ... ，本篇文長只會帶過基礎的SVM觀念以及些許 kernel function 的作用和原理。
  
<br>  

原本 logistic regression 中 **0 <= h(x) <=1** ，但現在改變策略為 **y*h(x)>=1**，
當 label y=1 時，h(x)必須大於等於1，若label y=-1時，h(x)必須小於等於-1。這裡暫時只討論**h<sub>&theta;</sub>(x) = &theta;<sup>T</sup>X**沒有像是logistic regression時有一個sigmoid非線性轉換。  

<br>  
  
根據這個策略 Cost funciton 也要稍微改動一下，為了讓假說值達成上述目的，只要假說值偏離1或-1越遠給予的損失值就會越大，而大於等於1或小於等於-1以外就無所謂。因此可以使用 Hinge Function 作為 Cost Function
  
<br>  

> ### **Modify Logistic Regression and use different strategy to calculate hypothesis value.**  

<br>  
  
There is plenty of detail in SVM, including hard-margin, soft-margin, kernel... Only basic concepts of support vectors and kernel function would be introduced in this blogs. 

<br>  

We now change the way to calculate the hypothesis value by letting **y*h(x)>=1**, which means we need h(x)>=1 or h(x)<=-1 depending on labels. Hence, the new version of cost function needs to punish if hypothesis values are far from 1 or -1. The hinge function is perfectly fitted in this situation.

<br>

---
## **Maximum Margin**

<br>

> **&theta;<sup>T</sup>X = u (dot product) v = P<sup>(i)</sup> * norm(w)**  
  
<br>  
  
可以把係數和變數的計算想像成是向量內積，而向量內積的意義又可以解釋成 &theta; 向量投影到 X 向量的長度或是 X 向量投影到 &theta;向量的長度，並且以此新理解去計算新版的假說值  

除此之外將 cost function 裡面 regularization term 的 coefficient 乘以倒數，附加到 error term 的係數，以此控制對於 maximum margin 的要求程度 (對於分錯類別的懲罰程度)。此係數及為 **C**  

<br>

We can think the relationship of &theta;<sup>T</sup>X is the same as the inner product of two vector, theta and X. And, it means the projection length of vector theta to vector X ( same as projecting x to theta ). We can use this projection length to calculate hypothesis value.

Furthermore, the cost funciton of SVM multiplies the reciprocal of the coefficient in the regularization term. So that the error term can have a coefficient C (equal to 1 / lambda) to control how much we penalize the missing classification.

圖一 圖二 小大 margin

---
## **Kernel**

當資料集為線性不可分的時候，我們可以為資料及多新增一些feature，並以此新 feature 當作分類依據。舉例來說多項式方程式就是藉由新增高次方項的新feature協助fitting。除了高次方項還有甚麼方式可以新增 feature 嗎? 有，例如距離，假如只有AB兩個點，距離A點愈近越有可能是A的類別，距離B點愈近，越有可能是B的類別，而計算AB的距離 (或是說影響能力) 以高斯分佈遞減。像這些計算新 feature 的公式就稱為 **kernel function**

<br>  
  
When data is linear inseparable, we would need to create new features to help us identifying labels. For example, ploynomial used higher order variables to fit dataset and equation. What else can we use to create features? We can utilize the distance. For example, if we only have A and B data, we can use the distance from A and B to guess their class. The equation we calculate the distance and the influence of the distance is gaussian equation. These new ways to create features is called the **kernel function**.    

<br>

---
## **Demonstration**
<br>  

* 點擊左側空白區域新增資料點
* 如欲切換為另一顏色，點擊 change 按鈕，並繼續新增資料點
* 輸入 C 和 選擇 kernel 以觀察不同收斂結果
* 點擊 Run 按鈕顯示計算結果
* 如欲清除資料點擊計算結果，請點擊 Clear  
  
<br>  
  
* hit the left hand side blank region to add data.
* Change to another color by clicking the change button. And keep adding data.
* Edit C and kernel funciton to observe different results.
* Click Run to show the results.
* Click Clear to clear data and results.


---
## **Future Work**
**TODO: Multiclass**
