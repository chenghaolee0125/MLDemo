from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
from .models import Post
from .classifier import LogisticRegression, SupportVectorMachine
import numpy as np
import json

# Create your views here.

# import logging
# logging.basicConfig(level=logging.DEBUG,
#                     format='%(asctime)s %(levelname)s %(message)s',
#                     datefmt='%Y-%m-%d %H:%M',
#                     handlers=[logging.FileHandler('log/MLDemo.log', 'w', 'utf-8'), ])
 


def home(request):
    post_list = Post.objects.all()
    # logging.debug('Visit Home')
    return render(request, 'home.html', {'post_list': post_list})
    # return HttpResponse("Hello World! /nThis is home page")

def test(request):
    return render(request, 'test.html', {'foo': 'bar'})
    
def add(request):
    a = request.GET['train_points']
    # print(a)
    b = json.loads(a)
    # c = sum([sum([int(i),int(j)]) for i,j in b])
    
    return HttpResponse(b)

def svm(request):
    post_list = Post.objects.all()
    return render(request, 'svm.html', {'post_list': post_list})

def randomforest(request):
    post_list = Post.objects.all()
    return render(request, 'randomforest.html', {'post_list': post_list})


def logistic(request):
    # a = LogisticRegression(context={"data": np.array([[1, 1], [2, 1], [-1, -1]]),
    #                                 "label": np.array([1, 1, 0])})
    # a.hypo_func()
    # a.calculate_cost()
    # ctx = """
    #         Hello World! \n
    #         This is logistic page! \n
    #         I have hypothesis value {} with initial data! \n
    #         Cost value = {} \n
    #         """.format(a.hypo_val, a.cost_val)
    # iteration = 10
    # ctx_iter = ""
    # for i in range(iteration):
    #     a.gradient_descent()
    #     ctx_iter += "After a new iteration, new theta = {}, new hypo_val = {}, and new cost = {} \n".format(
    #         a.theta, a.hypo_val, a.cost_val)
    # logging.debug('Visit logistic!')
    post_list = Post.objects.all()
    return render(request, 'logistic.html', {'post_list': post_list})
    # return HttpResponse(ctx+ctx_iter, content_type="text/plain")

def run_logistic(request):
    train_points_raw = request.GET['train_points']
    train_points_unjason = json.loads(train_points_raw)

    lr = float(request.GET['lr'])
    iteration = int(request.GET['iter'])

    # print(request_unjason)


    context = {}
    context["data"] = []
    context["label"] = []
    ##############
    # todo
    # add logistic function
    for i,j,k in train_points_unjason:
        context["data"].append([1, float(j), float(k)])
        context["label"].append(int(i))
    context["data"] = np.array(context["data"])
    context["label"] = np.array(context["label"])
    clf = LogisticRegression(context=context)
    clf.check_label_and_hypo_val()
    clf.hypo_func()
    clf.calculate_cost()
    
    for i in range(iteration):
        clf.gradient_descent(lr=lr)
        
    # logging.debug('Return Theta = {}'.format(clf.theta))
    return JsonResponse(clf.theta, safe=False)
    # return HttpResponse(clf.theta)



def run_svm(request):
    train_points_raw = request.POST['train_points']
    train_points_unjason = json.loads(train_points_raw)

    C = float(request.POST['C'])
    if C <= 0:
        C = 1
    kernel = str(request.POST['kernel'])

    # print(request_unjason)


    context = {}
    context["data"] = []
    context["label"] = []
    ##############
    # todo
    # add logistic function
    for i,j,k in train_points_unjason:
        context["data"].append([float(j), float(k)])
        context["label"].append(int(i))
    context["data"] = np.array(context["data"])
    context["label"] = np.array(context["label"])
    clf = SupportVectorMachine(context=context)
    clf.build_classifier(C=C,kernel=kernel)
    grid_results = clf.plot_grid()
    
    # logging.debug('Return Theta = {}'.format(clf.theta))
    return JsonResponse(grid_results, safe=False)


def home_test(request):
    # return HttpResponse("Hello World! /nThis is home page")
    post_list = Post.objects.all()
    return render(request, 'home_test.html', {'post_list': post_list})


def foo():
    pass

def ajax_list(request):
    a = list(range(100))
    return HttpResponse(json.dumps(a), content_type='application/json')

def ajax_dict(request):
    name_dict = {'twz': 'Love python and Django', 'zqxt': 'I am teaching Django'}
    return HttpResponse(json.dumps(name_dict), content_type='application/json')
