from django.db import models
import datetime
# Create your models here.


class Post(models.Model):
    title = models.CharField(max_length=25)
    # created_at = models.DateTimeField(auto_now_add=True)
    created_at = models.DateTimeField(datetime.date.today())
    def __str__(self):
        return self.title

    class Meta:
        ordering = ('-created_at',)
