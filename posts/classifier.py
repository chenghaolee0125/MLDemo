import numpy as np
from sklearn import svm

class LogisticRegression(object):
    np.random.seed(seed=None)

    def __init__(self, context={"data": None, "label": None}):
        self.data = context["data"]
        self.label = context["label"]
        self.theta = [round(elem, 3) for elem in np.random.random(3)]

    def check_label_and_hypo_val(self):
        self.hypo_func()
        total_hypo_val_label_0 = 0
        total_hypo_val_label_1 = 0
        for idx, label in enumerate(self.label):
            if label == 0:
                total_hypo_val_label_0 += self.hypo_val[idx]
            else:
                total_hypo_val_label_1 += self.hypo_val[idx]

        # print(total_hypo_val_label_0, total_hypo_val_label_1)
        if total_hypo_val_label_0 > total_hypo_val_label_1:
            self.label = 1-self.label

    def hypo_func(self):
        hypo_val = 1 / \
            (1+np.exp(-(np.dot(self.theta, self.data.transpose()))))
        # print(hypo_val)
        self.hypo_val = hypo_val

    def calculate_cost(self):
        cost = -(np.dot(self.label, np.log(self.hypo_val)) +
                 np.dot(1-self.label, np.log(1-self.hypo_val)))/len(self.data)
        # print(cost)
        self.cost_val = cost

    def gradient_descent(self, lr=0.1):
        # new_theta = old_theta - alpha*sigma from i in m ((hxi)-yi)*xji
        new_theta = []
        old_theta = self.theta
        old_hypo_val = self.hypo_val


        # j = [theta_0, theta_1, theta_2]
        for j in range(len(old_theta)):
            partial_theta_j = 0
            partial_theta_0 = 0
            # i = [data_0, data_1, data_2 ... ]
            if j == 0:
                for i in range(len(old_hypo_val)):
                    partial_theta_0 += (old_hypo_val[i] - self.label[i])
                new_theta.append(old_theta[j]-lr*partial_theta_0/len(old_hypo_val))
            else:
                for i in range(len(old_hypo_val)):
                    partial_theta_j += (old_hypo_val[i]-self.label[i])*self.data[i][j]
                new_theta.append(old_theta[j]-lr*partial_theta_j/len(old_hypo_val))
        self.theta = [round(elem, 3) for elem in np.array(new_theta)]
        self.hypo_func()
        self.calculate_cost()


class SupportVectorMachine(object):
    np.random.seed(seed=None)

    def __init__(self, context={"data": None, "label": None}):
        self.data = context["data"]
        self.label = context["label"]
        self._model = None
        self.xx, self.yy = np.meshgrid(np.arange(0, 1, 0.02), np.arange(0, 1, 0.02))

    def plot_grid(self):
        grid_data = [*zip(self.xx.flatten(), self.yy.flatten())]
        grid_results = self._model.predict(grid_data).tolist()
        return [*zip(grid_data, grid_results)]
        # return(grid_results)
    
    def build_classifier(self, C=1, kernel='linear'):
        clf = svm.SVC(C=C, kernel=kernel)
        # print(self.data, self.label)
        self._model = clf.fit(self.data, self.label) 
        